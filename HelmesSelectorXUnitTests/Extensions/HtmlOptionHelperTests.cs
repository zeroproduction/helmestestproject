﻿using HelmesSelector.Extensions;
using HelmesSelector.Models;
using System.Collections.Generic;
using Xunit;

namespace HelmesSelectorXUnitTests.Extensions
{
    public class HtmlOptionHelperTests
    {
        private readonly IEnumerable<Sector> _categoryModels;

        public HtmlOptionHelperTests()
        {
            _categoryModels = new List<Sector>()
            {
                new Sector()
                {
                    Name = "test1Parent",
                    Id = 1,
                    ChildSectors = new List<Sector>()
                    {
                        new Sector()
                        {
                            Id = 3,
                            Name = "test1ParentChild1",
                            ParentId = 1,
                            ChildSectors = new List<Sector>()
                        },
                        new Sector()
                        {
                            Id = 4,
                            Name = "test1ParentChild2",
                            ParentId = 1,
                            ChildSectors = new List<Sector>()
                        },
                    }
                },
                new Sector()
                {
                    Name = "test2Parent",
                    Id = 2,
                    ChildSectors = new List<Sector>()
                    {
                        new Sector()
                        {
                            Id = 5,
                            Name = "test2ParentChild1",
                            ParentId = 2,
                            ChildSectors = new List<Sector>()
                        },
                        new Sector()
                        {
                            Id = 6,
                            Name = "test2ParentChild2",
                            ParentId = 2,
                            ChildSectors = new List<Sector>()
                        },
                    }
                },
            };
        }

        [Fact]
        public void GetHtmlOptionElementsList_WithIndent_Null_Selected_NotNUll()
        {
            HtmlOptionHelper.BuildRows(
                _categoryModels, new List<UserSector>());
            var optonHtmlString = HtmlOptionHelper.GetOptionString;

            Assert.NotNull(optonHtmlString);
        }

        [Fact]
        public void GetHtmlOptionElementsList_WithIndent_One_Selected_NotNull()
        {
            HtmlOptionHelper.BuildRows(
                _categoryModels, new List<UserSector>()
                {
                    new UserSector()
                    {
                        SectorId = 5,
                    }
                });
            var optonHtmlString = HtmlOptionHelper.GetOptionString;

            Assert.NotNull(optonHtmlString);
            Assert.Contains("selected", optonHtmlString);
        }
    }
}
