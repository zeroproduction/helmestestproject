﻿using HelmesSelector.Models;
using Microsoft.EntityFrameworkCore;

namespace HelmesSelector.Data
{
    public class HelmesSelectorContext : DbContext
    {

        public DbSet<User> User { get; set; }
        public DbSet<UserSector> UserSector { get; set; }
        public DbSet<Sector> Sector { get; set; }

        public HelmesSelectorContext (DbContextOptions<HelmesSelectorContext> options)
            : base(options)
        {

        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {

            modelBuilder.Entity<UserSector>()
         .HasKey(pc => new { pc.UserId, pc.SectorId });

            modelBuilder.Entity<UserSector>()
                .HasOne(pc => pc.User)
                .WithMany(p => p.UserSectors)
                .HasForeignKey(pc => pc.UserId);

            modelBuilder.Entity<UserSector>()
                .HasOne(pc => pc.Sector)
                .WithMany(c => c.UserSectors)
                .HasForeignKey(pc => pc.SectorId);


            modelBuilder.Entity<Sector>().HasData(
                new Sector() { Id = 1, Name = "Manufacturing"},
                new Sector() { Id = 2, Name = "Construction materials", ParentId = 1 },
                new Sector() { Id = 3, Name = "Electronics and Optics", ParentId = 1 },
                new Sector() { Id = 4, Name = "Food and Beverage", ParentId = 1},
                new Sector() { Id = 5, Name = "Furniture", ParentId = 1 },
                new Sector() { Id = 6, Name = "Machinery", ParentId = 1 },
                new Sector() { Id = 7, Name = "Metalworking", ParentId = 1 },
                new Sector() { Id = 8, Name = "Plastic and Rubber", ParentId = 1 },
                new Sector() { Id = 9, Name = "Printing", ParentId = 1 },
                new Sector() { Id = 10, Name = "Textile and Clothing", ParentId = 1 },
                new Sector() { Id = 11, Name = "Wood", ParentId = 1 },

                /* Food and Beverage */

                new Sector() { Id = 12, Name = "Bakery & confectionery products", ParentId = 4 },
                new Sector() { Id = 13, Name = "Beverages", ParentId = 4 },
                new Sector() { Id = 14, Name = "Fish & fish products", ParentId = 4 },
                new Sector() { Id = 15, Name = "Meat & meat products", ParentId = 4 },
                new Sector() { Id = 16, Name = "Milk & dairy products", ParentId = 4 },
                new Sector() { Id = 17, Name = "Other", ParentId = 4 },
                new Sector() { Id = 18, Name = "Sweets & snack food", ParentId = 4 },

                /* Furniture */

                new Sector() { Id = 19, Name = "Bathroom/sauna", ParentId = 5 },
                new Sector() { Id = 20, Name = "Bedroom", ParentId = 5 },
                new Sector() { Id = 21, Name = "Children’s room", ParentId = 5 },
                new Sector() { Id = 22, Name = "Kitchen", ParentId = 5 },
                new Sector() { Id = 23, Name = "Living room", ParentId = 5 },
                new Sector() { Id = 24, Name = "Office", ParentId = 5 },
                new Sector() { Id = 25, Name = "Other (Furniture)", ParentId = 5 },
                new Sector() { Id = 26, Name = "Outdoor", ParentId = 5 },
                new Sector() { Id = 27, Name = "Project furniture", ParentId = 5 },

                /* Machinery */

                new Sector() { Id = 28, Name = "Machinery components", ParentId = 6 },
                new Sector() { Id = 29, Name = "Machinery equipment/tools", ParentId = 6 },
                new Sector() { Id = 30, Name = "Manufacture of machinery", ParentId = 6 },
                new Sector() { Id = 31, Name = "Maritime", ParentId = 6 },
                new Sector() { Id = 32, Name = "Aluminium and steel workboats", ParentId = 32 },
                new Sector() { Id = 33, Name = "Boat/Yacht building", ParentId = 32 },
                new Sector() { Id = 34, Name = "Ship repair and conversion", ParentId = 32 },
                new Sector() { Id = 35, Name = "Metal structures", ParentId = 6 },
                new Sector() { Id = 36, Name = "Other", ParentId = 6 },
                new Sector() { Id = 37, Name = "Repair and maintenance service", ParentId = 6 },

                /* Metalworking */

                new Sector() { Id = 38, Name = "Construction of metal structures", ParentId = 7 },
                new Sector() { Id = 39, Name = "Houses and buildings", ParentId = 7 },
                new Sector() { Id = 40, Name = "Metal products", ParentId = 7 },
                new Sector() { Id = 41, Name = "Metal works", ParentId = 7 },
                new Sector() { Id = 42, Name = "CNC-machining", ParentId = 41 },
                new Sector() { Id = 43, Name = "Forgings, Fasteners", ParentId = 41 },
                new Sector() { Id = 44, Name = "Gas, Plasma, Laser cutting", ParentId = 41 },
                new Sector() { Id = 45, Name = "MIG, TIG, Aluminum welding", ParentId = 41 },


                /* Plastic and Rubber */

                new Sector() { Id = 46, Name = "Packaging", ParentId = 8 },
                new Sector() { Id = 47, Name = "Plastic goods", ParentId = 8 },
                new Sector() { Id = 48, Name = "Plastic processing technology", ParentId = 8 },
                new Sector() { Id = 49, Name = "Blowing", ParentId = 48 },
                new Sector() { Id = 50, Name = "Moulding", ParentId = 48 },
                new Sector() { Id = 51, Name = "Plastics welding and processing", ParentId = 48 },
                new Sector() { Id = 52, Name = "Plastic profiles", ParentId = 8 },

                /* Printing */
                new Sector() { Id = 53, Name = "Advertising", ParentId = 9 },
                new Sector() { Id = 54, Name = "Book/Periodicals printing", ParentId = 9 },
                new Sector() { Id = 55, Name = "Labelling and packaging printing", ParentId = 9 },

                /* Textile and Clothing */
                new Sector() { Id = 56, Name = "Advertising", ParentId = 10 },
                new Sector() { Id = 57, Name = "Book/Periodicals printing", ParentId = 10 },

                /* Wood */
                new Sector() { Id = 58, Name = "Advertising", ParentId = 11 },
                new Sector() { Id = 59, Name = "Book/Periodicals printing", ParentId = 11 },
                new Sector() { Id = 60, Name = "Labelling and packaging printing", ParentId = 11 },


                /* Other */
                new Sector() { Id = 61, Name = "Other" },
                new Sector() { Id = 62, Name = "Creative industries", ParentId = 61 },
                new Sector() { Id = 63, Name = "Energy technology", ParentId = 61 },
                new Sector() { Id = 64, Name = "Environment", ParentId = 61 },

                /* Services */
                new Sector() { Id = 65, Name = "Service" },
                new Sector() { Id = 66, Name = "Business services", ParentId = 65 },
                new Sector() { Id = 67, Name = "Engineering", ParentId = 65 },
                new Sector() { Id = 68, Name = "Information Technology and Telecommunications", ParentId = 65 }, /* Parent */
                new Sector() { Id = 69, Name = "Data processing, Web portals, E-marketing", ParentId = 68 },
                new Sector() { Id = 70, Name = "Programming, Consultancy", ParentId = 68 },
                new Sector() { Id = 71, Name = "Software, Hardware", ParentId = 68 },
                new Sector() { Id = 72, Name = "Telecommunications", ParentId = 68 },
                new Sector() { Id = 73, Name = "Tourism", ParentId = 65 },
                new Sector() { Id = 74, Name = "Translation services", ParentId = 65 },
                new Sector() { Id = 75, Name = "Transport and Logistics", ParentId = 65 }, /* Parent */
                new Sector() { Id = 76, Name = "Air", ParentId = 75 },
                new Sector() { Id = 77, Name = "Rail", ParentId = 75 },
                new Sector() { Id = 78, Name = "Road", ParentId = 75 },
                new Sector() { Id = 79, Name = "Water", ParentId = 75 }

            );
        }
    }
}
