﻿using HelmesSelector.Models;
using Microsoft.AspNetCore.Html;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HelmesSelector.Extensions
{
    public static class HtmlOptionHelper
    {
        private static string _optionString;

        public static string GetOptionString
        {
            get => _optionString;
        }



        private static List<UserSector> _selectedUserSectorsModel;

        public static IHtmlContent GetHtmlOptionElementsList(IEnumerable<Sector> model, 
                                                             List<UserSector> userModel)
        {
            BuildRows(model, userModel);
            return new HtmlContentBuilder().AppendHtml(_optionString);
        }

        public static void BuildRows(IEnumerable<Sector> model,
                                     List<UserSector> userModel)
        {
            _selectedUserSectorsModel = userModel;

            foreach (var item in model)
            {
                _buildOptionTag(item);
                _buildOptionRow(item);
            }
        }

        private static void _buildOptionRow(Sector sector, int level = 1)
        {
            foreach (var item in sector.ChildSectors)
            {
                
                for (int i = 0; i < 4 * level; i++)
                {
                    item.Name = HttpUtility.HtmlDecode("&nbsp;") + item.Name;
                }
       
                _buildOptionTag(item);
                if (item.ChildSectors.Count > 0)
                {
                    _buildOptionRow(item, level + 1);
                    //continue;
                }



            }
        }

        private static void _buildOptionTag(Sector sector)
        {
            var hasSectorId = _selectedUserSectorsModel.FirstOrDefault(x => x.SectorId == sector.Id);

            var valueAttr = $"value={sector.Id}";
            var attributes = hasSectorId != null ? $"{valueAttr} selected" : valueAttr;
            _optionString += $"<option {attributes}>{sector.Name}</option>";
        }
    }
}
