﻿using HelmesSelector.Data;
using HelmesSelector.Models;
using HelmesSelector.Services.Interfaces;
using Microsoft.AspNetCore.Mvc;

namespace HelmesSelector.Controllers.Api
{
    [Route("api/ApiForm")]
    [ApiController]
    public class ApiFormController : ControllerBase
    {
        private readonly IUserService _userService;
        private readonly ISectorService _sectorService;

        public ApiFormController(HelmesSelectorContext context, 
            IUserService userService, ISectorService sectorService)
        {
            _userService = userService;
            _sectorService = sectorService;
            context.Database.EnsureCreated();
        }

        [HttpPost]
        public JsonResult Index([FromBody] SaveRequestModel saveRequestModel)
        {
            if (!ModelState.IsValid)
            {
                return new JsonResult(new
                {
                    HasErrors = true,
                    Content = "Not Valid Data",
                });
            }

            
            _userService.AddOrUpdateUserData(saveRequestModel, HttpContext.Session);
            //_sectorService.SaveOrUpdateUserSector(saveRequestModel);

            return new JsonResult(new { HasErrors = false });
        }
    }
}
