﻿using HelmesSelector.Data;
using HelmesSelector.Models;
using HelmesSelector.Services.Interfaces;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace HelmesSelector.Controllers
{
    public class CategoryController : Controller
    {
        private readonly IUserService _userService;
        private readonly ISectorService _sectorService;
        private readonly ICategoryService _categoryService;

        public CategoryController(HelmesSelectorContext context, IUserService userService, 
                                  ISectorService sectorService, ICategoryService categoryService)
        {
            _userService = userService;
            _sectorService = sectorService;
            _categoryService = categoryService;
            context.Database.EnsureCreated();
        }

        // GET: Categories
        public async Task<IActionResult> Index()
        {
            var categoriesModel = _categoryService.GetCategoryModels();

            var userId = HttpContext.Session.GetString("UserId");
            User userModel;
            if (userId == null)
            {
                userModel = _userService.GetUserData(null);
            }
            else
            {
                userModel = _userService.GetUserData(Convert.ToInt32(userId));
            }

            List<UserSector> sectorsModel = new List<UserSector>();

            if(userModel.Id != 0)
            {
                sectorsModel = await _sectorService.GetAllSectors(userModel.Id);
            }
           

            return View(new CategoriesViewModel()
            {
                User = userModel,
                CategoryModel = categoriesModel,
                UserSectorsModel = sectorsModel
            });
        }
    }
}
