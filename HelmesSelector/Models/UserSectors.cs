﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace HelmesSelector.Models
{
    public class UserSector
    {
        public int SectorId { get; set; }
        public int UserId { get; set; }

        public Sector Sector { get; set; }
        public User User { get; set; }
    }
}
