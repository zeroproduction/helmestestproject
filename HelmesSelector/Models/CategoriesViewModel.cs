﻿using System.Collections.Generic;

namespace HelmesSelector.Models
{
    public class CategoriesViewModel
    {
        public User User { get; set; }
        public List<UserSector> UserSectorsModel { get; set; }
        public List<Sector> CategoryModel { get; set; }
    }
}
