﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace HelmesSelector.Models
{
    public class Sector
    {
        [Key, Column(Order = 0)]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        [ForeignKey("Id"), Column(Order = 1)]
        public int? ParentId { get; set; }

        public string Name { get; set; }

        public virtual Sector Parent { get; set; }
        public virtual List<Sector> ChildSectors { get; set; }
        public virtual ICollection<UserSector> UserSectors { get; set; }
    }
}
