﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace HelmesSelector.Models
{
    public class User
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Key, Column(Order = 0)]
        public int Id { get; set; }

        public string Name { get; set; }

        [NotMapped]
        public List<int> Sectors { get; set; }

        public bool AgreementToTerms { get; set; } = false;

        public virtual ICollection<UserSector> UserSectors { get; set; }
    }
}
