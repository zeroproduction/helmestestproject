﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Newtonsoft.Json;

namespace HelmesSelector.Models
{
    public class SaveRequestModel
    {
        [Required]
        [MinLength(4)]
        [JsonProperty("name")]
        public string Name { get; set; }

        [Required]
        [JsonProperty("sectors")]
        public List<int> Sectors { get; set; }

        [Required]
        [JsonProperty("agreement")]
        public bool AgreementToTerms { get; set; }
    }
}
