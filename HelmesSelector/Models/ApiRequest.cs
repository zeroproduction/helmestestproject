﻿namespace HelmesSelector.Models
{
    public class ApiRequest
    {
        public bool HasErrors { get; set; }
        public string Content { get; set; }
    }
}
