﻿using HelmesSelector.Data;
using HelmesSelector.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HelmesSelector.Helpers
{
    public class SectorServiceHelper
    {

        private readonly HelmesSelectorContext _helmesSelectorContext;

        public SectorServiceHelper(HelmesSelectorContext helmesSelectorContext)
        {
            _helmesSelectorContext = helmesSelectorContext;
        }

        public async Task<List<UserSector>> GetUserSectorsData(int id)
        {
            try
            {
                return await _helmesSelectorContext.UserSector.Where(x => x.UserId == id).ToListAsync();
            }
            catch (Exception e)
            {
                ExceptionHelper.HandleException(e);
                return new List<UserSector>();
            }
        }
    }
}
