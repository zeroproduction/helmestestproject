﻿using HelmesSelector.Helpers;
using HelmesSelector.Models;
using HelmesSelector.Services.Interfaces;
using System.Threading.Tasks;
using HelmesSelector.Data;
using System.Collections.Generic;

namespace HelmesSelector.Services
{
    public class UserService : IUserService
    {
        private readonly UserServiceHelper _userServiceHelper;

        public UserService(HelmesSelectorContext helmesSelectorContext)
        {
            _userServiceHelper = new UserServiceHelper(helmesSelectorContext);
        }

        public User GetUserData(int? userId)
        {
            return _userServiceHelper.GetUserData(userId);
        }

        public void AddOrUpdateUserData(SaveRequestModel saveRequestModel, Microsoft.AspNetCore.Http.ISession session)
        {
            var user = new User()
            {
                Name = saveRequestModel.Name,
                AgreementToTerms = saveRequestModel.AgreementToTerms,
                UserSectors = new List<UserSector>()
            };

            foreach (var sectorId in saveRequestModel.Sectors)
            {
                user.UserSectors.Add(new UserSector() { SectorId = sectorId });
            }

            Task.Run(async () => await _userServiceHelper.AddOrUpdateUserData(user, session)).Wait();
        }
    }
}
