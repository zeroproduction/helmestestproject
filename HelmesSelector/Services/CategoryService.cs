﻿using System.Collections.Generic;
using HelmesSelector.Data;
using HelmesSelector.Helpers;
using HelmesSelector.Models;
using HelmesSelector.Services.Interfaces;

namespace HelmesSelector.Services
{
    public class CategoryService : ICategoryService
    {
        private readonly CategoryServiceHelper _categoryServiceHelper;

        public CategoryService(HelmesSelectorContext helmesSelectorContext)
        {
            _categoryServiceHelper = new CategoryServiceHelper(helmesSelectorContext);
        }

        public List<Sector> GetCategoryModels()
        {
            return _categoryServiceHelper.GetCategoryModels();
        }
    }
}
