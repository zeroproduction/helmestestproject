﻿using HelmesSelector.Models;
using HelmesSelector.Services.Interfaces;
using System.Collections.Generic;
using System.Threading.Tasks;
using HelmesSelector.Data;
using HelmesSelector.Helpers;

namespace HelmesSelector.Services
{
    public class SectorService : ISectorService
    {
        private readonly SectorServiceHelper _sectorServiceHelper;

        public SectorService(HelmesSelectorContext helmesSelectorContext)
        {
            _sectorServiceHelper = new SectorServiceHelper(helmesSelectorContext);
        }

        public async Task<List<UserSector>> GetAllSectors(int id)
        {
            return await _sectorServiceHelper.GetUserSectorsData(id);
        }
    }
}
