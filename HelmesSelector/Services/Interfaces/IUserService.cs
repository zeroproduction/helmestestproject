﻿using System.Threading.Tasks;
using HelmesSelector.Models;

namespace HelmesSelector.Services.Interfaces
{
    public interface IUserService
    {
        User GetUserData(int? userId);
        void AddOrUpdateUserData(SaveRequestModel saveRequestModel, Microsoft.AspNetCore.Http.ISession session);
    }
}
