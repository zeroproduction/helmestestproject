﻿using System.Collections.Generic;
using HelmesSelector.Models;

namespace HelmesSelector.Services.Interfaces
{
    public interface ICategoryService
    {
        List<Sector> GetCategoryModels();
    }
}
