﻿using System.Collections.Generic;
using System.Threading.Tasks;
using HelmesSelector.Models;

namespace HelmesSelector.Services.Interfaces
{
    public interface ISectorService
    {
        Task<List<UserSector>> GetAllSectors(int id);
    }
}
